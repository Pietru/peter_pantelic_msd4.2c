﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class menuSoundScript : MonoBehaviour {

	//public variables since we're only linking one button; both image files will need to be assigned manually
	public Button soundToggle;
	public Sprite soundOn;
	public Sprite soundOff;
	//the counter variable will be used in the function 'toggleSound'
	//private because it will only be used in this code
	private int counter = 0;

	AudioSource bgm;
	public AudioClip sound;


	// Use this for initialization
	void Start () {
		//finding soundToggle in the Unity UI
		soundToggle = GameObject.Find ("soundToggle").GetComponent<Button> ();
		soundToggle.onClick.AddListener (toggleSound);
	}

	void toggleSound()
	//if the counter value is not divisible by 2, the sound will be off
	//if the counter value IS divisible by 2, the sound will be on
	{
		counter++;
		if (counter % 2 == 0) {
			soundToggle.image.overrideSprite = soundOn;
			//play sound/music
			bgm = GetComponent<AudioSource>();
			bgm.Play();
		} 
		else {
			soundToggle.image.overrideSprite = soundOff;
			//stop playing sound/music
			bgm = GetComponent<AudioSource>();
			bgm.Stop();
		}
	}


	
	// Update is called once per frame
	void Update () {
		
	}
}
