﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blockScript : MonoBehaviour {

	float falling = 0;
	public float fallingSpeed = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		userInput ();
	}

	//method holding the user controls
	void userInput(){
		if (Input.GetKeyDown (KeyCode.UpArrow)) {

			//move the block one unit up
			Debug.Log ("Input: Up arrow");
			transform.position += new Vector3 (0, 1, 0);

			if (transform.position.y <= 3) {
				//do nothing
			} else {
				transform.position += new Vector3 (0, -1, 0);
			}

		} else if (Input.GetKeyDown (KeyCode.DownArrow)) {

			//move the block one unit down
			Debug.Log ("Input: Down arrow");
			transform.position += new Vector3 (0, -1, 0);

			if (transform.position.y >= -4.5) {
				//do nothing
			} else {
				transform.position += new Vector3 (0, 1, 0);
			}
		} else if (Input.GetKeyDown (KeyCode.LeftArrow)) {

			//rotate the block once by 90 degrees
			Debug.Log ("Input: Left arrow");
			transform.Rotate (0, 0, 90);
			//add SFX

			if ((transform.position.y >= -4.5) && (transform.position.y <= 3) && (transform.position.x <=8.5)) {
				//do nothing
			} else{
				transform.Rotate(0,0,-90);
			}

		} else if (Input.GetKeyDown (KeyCode.RightArrow) || Time.time - falling >= fallingSpeed) {

			//move the block one unit right if right key is pressed
			//or whenever one second in the game passes (Time.time)
			Debug.Log ("Input: Right arrow");
			transform.position += new Vector3 (1, 0, 0);

			//variable 'falling' increases by 1 each second
			falling = Time.time;

			if (transform.position.x <= 8.5) {
				//do nothing
			} else {
				transform.position += new Vector3 (-1, 0, 0);
				//when the block falls to the bottom, disable the block so that another block can be spawned
				enabled = false;
			}
		}
	}
}



