﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menuScript : MonoBehaviour {

	//no need for public since every button listed is linked to the script itself
	Button startButton;
	Button instructionsButton;
	Button highscoresButton;

	// Use this for initialization
	void Start () {
		//finding each specific component from the Unity UI
		startButton = GameObject.Find ("startButton").GetComponent<Button> ();
		startButton.onClick.AddListener (startGame);
		instructionsButton = GameObject.Find ("instructionsButton").GetComponent<Button> ();
		instructionsButton.onClick.AddListener (openInstructions);
		highscoresButton = GameObject.Find ("highscoresButton").GetComponent<Button> ();
		highscoresButton.onClick.AddListener (openHighscores);
	}

	void startGame()
	//loads the scene 'Level' (the game)
	{
		SceneManager.LoadScene("Level");
	}

	void openInstructions()
	//opens up an HTML webpage displaying the instructions
	{
		Application.OpenURL ("http://www.google.com");
	}

	void openHighscores()
	//loads the scene 'Highscores'
	{
		SceneManager.LoadScene("Highscores");
	}


	
	// Update is called once per frame
	void Update () {
		
	}
}
