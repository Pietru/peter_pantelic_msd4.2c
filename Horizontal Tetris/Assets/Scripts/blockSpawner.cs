﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blockSpawner : MonoBehaviour {

	public GameObject[] blocks;

	public void spawnBlock()
	{
		int shapeIndex = Random.Range(0, 6);

		//spawn a block randomly from a list of 7 blocks
		Instantiate(blocks[shapeIndex],
			transform.position,
			Quaternion.identity);
	}

	// Use this for initialization
	void Start () {
		spawnBlock();
	}

	// Update is called once per frame
	void Update () {

	}
}
